/**
 * Automatically generated file. DO NOT MODIFY
 */
package swati4star.createpdf;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "swati4star.createpdf";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 108;
  public static final String VERSION_NAME = "8.8.1";
}
