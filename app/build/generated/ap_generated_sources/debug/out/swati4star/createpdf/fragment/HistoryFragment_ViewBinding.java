// Generated code from Butter Knife. Do not modify!
package swati4star.createpdf.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import swati4star.createpdf.R;

public class HistoryFragment_ViewBinding implements Unbinder {
  private HistoryFragment target;

  private View view2131296471;

  @UiThread
  public HistoryFragment_ViewBinding(final HistoryFragment target, View source) {
    this.target = target;

    View view;
    target.mEmptyStatusLayout = Utils.findRequiredViewAsType(source, R.id.emptyStatusView, "field 'mEmptyStatusLayout'", ConstraintLayout.class);
    target.mHistoryRecyclerView = Utils.findRequiredViewAsType(source, R.id.historyRecyclerView, "field 'mHistoryRecyclerView'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.getStarted, "method 'loadHome'");
    view2131296471 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.loadHome();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    HistoryFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mEmptyStatusLayout = null;
    target.mHistoryRecyclerView = null;

    view2131296471.setOnClickListener(null);
    view2131296471 = null;
  }
}
