// Generated code from Butter Knife. Do not modify!
package swati4star.createpdf.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import swati4star.createpdf.R;

public class PreviewActivity_ViewBinding implements Unbinder {
  private PreviewActivity target;

  @UiThread
  public PreviewActivity_ViewBinding(PreviewActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PreviewActivity_ViewBinding(PreviewActivity target, View source) {
    this.target = target;

    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'mRecyclerView'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PreviewActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRecyclerView = null;
  }
}
