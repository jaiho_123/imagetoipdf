// Generated code from Butter Knife. Do not modify!
package swati4star.createpdf.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import swati4star.createpdf.R;

public class WelcomeActivity_ViewBinding implements Unbinder {
  private WelcomeActivity target;

  private View view2131296321;

  @UiThread
  public WelcomeActivity_ViewBinding(WelcomeActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public WelcomeActivity_ViewBinding(final WelcomeActivity target, View source) {
    this.target = target;

    View view;
    target.mViewPager = Utils.findRequiredViewAsType(source, R.id.view_pager, "field 'mViewPager'", ViewPager.class);
    target.mDotsLayout = Utils.findRequiredViewAsType(source, R.id.layoutDots, "field 'mDotsLayout'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.btn_skip, "field 'mBtnSkip' and method 'openMainActivity'");
    target.mBtnSkip = Utils.castView(view, R.id.btn_skip, "field 'mBtnSkip'", Button.class);
    view2131296321 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.openMainActivity();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    WelcomeActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mViewPager = null;
    target.mDotsLayout = null;
    target.mBtnSkip = null;

    view2131296321.setOnClickListener(null);
    view2131296321 = null;
  }
}
