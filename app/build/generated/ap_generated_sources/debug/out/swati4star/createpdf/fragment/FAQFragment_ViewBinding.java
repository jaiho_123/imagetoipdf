// Generated code from Butter Knife. Do not modify!
package swati4star.createpdf.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import swati4star.createpdf.R;

public class FAQFragment_ViewBinding implements Unbinder {
  private FAQFragment target;

  @UiThread
  public FAQFragment_ViewBinding(FAQFragment target, View source) {
    this.target = target;

    target.mFAQRecyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view_faq, "field 'mFAQRecyclerView'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FAQFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mFAQRecyclerView = null;
  }
}
