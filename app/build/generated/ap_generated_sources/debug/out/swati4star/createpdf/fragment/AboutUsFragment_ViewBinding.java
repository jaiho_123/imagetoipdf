// Generated code from Butter Knife. Do not modify!
package swati4star.createpdf.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import swati4star.createpdf.R;

public class AboutUsFragment_ViewBinding implements Unbinder {
  private AboutUsFragment target;

  private View view2131296514;

  private View view2131296520;

  private View view2131296519;

  private View view2131296515;

  private View view2131296513;

  private View view2131296517;

  private View view2131296518;

  private View view2131296516;

  @UiThread
  public AboutUsFragment_ViewBinding(final AboutUsFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.layout_email, "method 'sendmail'");
    view2131296514 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.sendmail();
      }
    });
    view = Utils.findRequiredView(source, R.id.layout_website, "method 'openWeb'");
    view2131296520 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.openWeb();
      }
    });
    view = Utils.findRequiredView(source, R.id.layout_slack, "method 'joinSlack'");
    view2131296519 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.joinSlack();
      }
    });
    view = Utils.findRequiredView(source, R.id.layout_github, "method 'githubRepo'");
    view2131296515 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.githubRepo();
      }
    });
    view = Utils.findRequiredView(source, R.id.layout_contri, "method 'contributorsList'");
    view2131296513 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.contributorsList();
      }
    });
    view = Utils.findRequiredView(source, R.id.layout_playstore, "method 'openPlaystore'");
    view2131296517 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.openPlaystore();
      }
    });
    view = Utils.findRequiredView(source, R.id.layout_privacy, "method 'privacyPolicy'");
    view2131296518 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.privacyPolicy();
      }
    });
    view = Utils.findRequiredView(source, R.id.layout_license, "method 'license'");
    view2131296516 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.license();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    target = null;


    view2131296514.setOnClickListener(null);
    view2131296514 = null;
    view2131296520.setOnClickListener(null);
    view2131296520 = null;
    view2131296519.setOnClickListener(null);
    view2131296519 = null;
    view2131296515.setOnClickListener(null);
    view2131296515 = null;
    view2131296513.setOnClickListener(null);
    view2131296513 = null;
    view2131296517.setOnClickListener(null);
    view2131296517 = null;
    view2131296518.setOnClickListener(null);
    view2131296518 = null;
    view2131296516.setOnClickListener(null);
    view2131296516 = null;
  }
}
