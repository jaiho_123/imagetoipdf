// Generated code from Butter Knife. Do not modify!
package swati4star.createpdf.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import swati4star.createpdf.R;

public class BrushItemAdapter$BrushItemViewHolder_ViewBinding implements Unbinder {
  private BrushItemAdapter.BrushItemViewHolder target;

  @UiThread
  public BrushItemAdapter$BrushItemViewHolder_ViewBinding(BrushItemAdapter.BrushItemViewHolder target,
      View source) {
    this.target = target;

    target.doodleButton = Utils.findRequiredViewAsType(source, R.id.doodle_color, "field 'doodleButton'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BrushItemAdapter.BrushItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.doodleButton = null;
  }
}
