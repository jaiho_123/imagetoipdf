// Generated code from Butter Knife. Do not modify!
package swati4star.createpdf.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import swati4star.createpdf.R;

public class RearrangeImages_ViewBinding implements Unbinder {
  private RearrangeImages target;

  private View view2131296723;

  @UiThread
  public RearrangeImages_ViewBinding(RearrangeImages target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RearrangeImages_ViewBinding(final RearrangeImages target, View source) {
    this.target = target;

    View view;
    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'mRecyclerView'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.sort, "method 'sortImg'");
    view2131296723 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.sortImg();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    RearrangeImages target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRecyclerView = null;

    view2131296723.setOnClickListener(null);
    view2131296723 = null;
  }
}
